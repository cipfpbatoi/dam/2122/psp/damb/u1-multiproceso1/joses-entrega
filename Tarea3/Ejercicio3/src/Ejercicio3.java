import java.io.*;
import java.util.ArrayList;

public class Ejercicio3 {
    public static void main(String[] args) {

        File file = new File(".\\..\\Minusculas");

        ArrayList<String> lista = new ArrayList<>();
        lista.add("cmd.exe");
        lista.add("/C");
        lista.add("java -jar target\\Minusculas-1.0-SNAPSHOT.jar HOLA ME LLAMO FINALIZAR JOSE");

        ProcessBuilder pb = new ProcessBuilder(lista);
        pb.command(lista).directory(file);

        try {
            Process proceso = pb.start();
            //esta parte hace que se pueda leer el resultado de ejecutar Minusculas
            InputStream is = proceso.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            //y así lo sacamos por pantalla
            String linea;
            while ((linea = br.readLine()) != null) {
                System.out.println(linea);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
