package es.cipfpbatoi.ad2122;

import java.util.Locale;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) {
        /**
         * Se han procesado correctamente los argumentos,
         * Se podría haber realizado algo similar en el ejercicio2
         * 
         * El ejercicio, pide lo mismo que el de los números aleatorios,
         * En lugar de introducirlo por la línea de comandos, se ha 
         * de introducir por el teclado cada vez.
         * 
         */
        for (int i = 0; i < args.length; i++) {
            if(args[i].equalsIgnoreCase("finalizar")) System.exit(0);
            System.out.print((args[i] + " ").toLowerCase());
        }
    }
}
