
import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Ejercicio1 {

    /**
     * ¿Todo esto era necesario?
     */
    public static File f = new File(".");
    public static File file = new File(f.getAbsolutePath().replace(".", ""), "output.txt");
    public static BufferedWriter bw;

    public static void main(String[] args) {

        ArrayList<String> lista = new ArrayList<>();

        /**
         * Y si ponemos más de 3 parámetros?
         */

        lista.add(args[0]);
        lista.add(args[1]);
        lista.add(args[2]);

        ProcessBuilder pb = new ProcessBuilder(lista);
        
        /**
         * Esto debería ser parte de la documentación del programa
         */
        // argumentos para pasarle: "cmd.exe", "/C", "dir"

        try {
            FileWriter fw = new FileWriter(file);
            bw = new BufferedWriter(fw);

            Process proceso = pb.start();
            
            // Lectura Entrada Estándar
            InputStream is = proceso.getInputStream();
            Scanner sc = new Scanner(is);

            // Flujo hacia el proceso
            OutputStream os = proceso.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os);
            BufferedWriter bfw = new BufferedWriter(osw);
            bfw.write(sc.nextLine());
            bfw.newLine();

           /**
            * 
            Estas líneas están intercambiadas
            */
            int exitCode = proceso.waitFor();
            boolean procesoHaFinalizado = proceso.waitFor(2, TimeUnit.SECONDS);

            if (exitCode != 0){
                System.err.println("Error en Proceso");
                System.exit(0);
            }

            if (procesoHaFinalizado == true && exitCode == 0) {
                while (sc.hasNext()) {
                    if (!file.exists()) file.createNewFile();
                    System.out.println(sc.nextLine());
                    bw.write(sc.nextLine());
                    /**
                     * En general, se desaconseja por la plataforma 
                     * utilizar la escritura del salto de línea.
                     * 
                     * Como has creado una instancia de BufferedWriter
                     * Ahora puedes utilizar el método
                     * 
                     * bw.newLine(); 
                     * 
                     * en lugar de agregar el salto de línea manualmente
                     * 
                     */
                    bw.write("\n");
                }
            }
            bw.close();
        } catch (IOException e) {
            System.err.println("Ha ocurrido un error de entrada y/o salida.");
        } catch (InterruptedException e) {
            System.err.println("Error. El hilo se ha interrumpido.");
        }
    }
}
