package es.cipfpbatoi.ad2122;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;


/**
 * Hello world!
 *
 */
public class App 
{
    public static File f = new File(".");
    public static File file = new File(f.getAbsolutePath().replace(".", ""), "randoms.txt");

    //Ambas son la misma ruta, la primera es absoluta y la segunda relativa
    public static File fil = new File("C:\\Users\\Jouss\\Desktop\\2º DAM\\Asignaturas\\PSP\\GITLAB\\bloque1\\Tarea2\\Ramdom10");
    public static File fi = new File(".\\..\\Ramdom10");


    public static void main( String[] args ) {
        //creamos un ArrayList con los comandos que le vamos a pasar como parámetro al ProcessBuilder
        ArrayList<String> lista = new ArrayList<>();
        lista.add("cmd.exe");
        lista.add("/C");
        lista.add("java -jar target\\Ramdom10-1.0-SNAPSHOT.jar ola k ase");
        
        //creamos el ProcessBuilder pasándole el ArrayList como parámentro e indicándole la ruta desde
        // donde vamos a ejecutar los comandos
        ProcessBuilder pb = new ProcessBuilder();
        pb.command(lista).directory(fi);

        try {
            Process proceso = pb.start();
            //esta parte hace que se pueda leer el resultado de ejecutar Ramdom10
            InputStream is = proceso.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            //esta otra parte escribe el resultado en el fichero randomms.txt
            FileWriter fw = new FileWriter(file);

            /**
             * Si en el ejercicio 1 utilizas un BufferedWriter para el 
             * fichero, ¿por qué aquí no?
             */

            String linea;
            while ((linea = br.readLine()) != null) {
                if (!file.exists()) file.createNewFile();
                System.out.println(linea);
                fw.write(linea);
                /**
                 * Esto va a dar una excepción
                 * No se puede cerrar cada vez el fichero
                 */
                fw.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
